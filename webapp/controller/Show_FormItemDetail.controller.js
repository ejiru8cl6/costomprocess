sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nNext/Flow7/Processing/model/Formatter"
], function(Controller, Formatter) {
	"use strict";

	return Controller.extend("nNext.Flow7.Processing.controller.Show_FormItemDetail", {
		oFormatter: Formatter,
		onInit: function(oEvent) {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(function(oEvent) {
				var requisitionId = oEvent.getParameter("arguments").requisitionId;
				$.ajax("/Flow7Api/api/fdp/m/" + requisitionId)
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						self.getView().setModel(oData, "formDetail");
					});
			});
		}
		
	});
});