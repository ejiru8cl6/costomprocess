sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/core/routing/History"
], function(Controller, MessageToast, History) {
	"use strict";

	return Controller.extend("nNext.Flow7.Processing.controller.Write_Form", {
		onInit: function() {
			self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Flow7", "FORM_DATA_CHANGE", this.onFormDataChange, this);
		},
		onFormDataChange: function(sChanel, sEvent, oData) {
			this.getView().setModel(oData, "storedData");
		},

		onFormSubmit: function() {
			var formData = this.getView().getModel("storedData").getJSON();

			var create = function() {
				var def = $.Deferred();

				$.ajax({
					url: "/Flow7Api/api/fdp/m",
					type: "POST",
					data: formData,
					contentType: "application/json",
					success: function() {
						def.resolve();
					},
					fail: function(data) {
						def.reject(data);
					}
				});

				return def.promise();
			};

			var nginStart = function() {
				var def = $.Deferred();

				$.ajax({
					url: "/Flow7Api/api/engine/start",
					type: "POST",
					data: formData,
					contentType: "application/json",
					success: function() {
						def.resolve();
					},
					fail: function(data) {
						def.reject(data);
					}
				});

				return def.promise();
			}

			create().then(nginStart())
				.done(function() {
					self.updatePage();
				})
				.fail(data => console.log(data));
		},

		updatePage: function() {
			MessageToast.show("表單已成功送出, 請至進行中表單查詢");
			this._oRouter.navTo("Show_Folder");
		},
		onBackPress: function(oEvent) {
			this._oRouter.navTo("Show_Folder");
		}
	});
});