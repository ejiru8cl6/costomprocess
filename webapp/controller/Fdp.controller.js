sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"nNext/Flow7/Processing/3rd/underscore"
], function(Controller,MessageToast, underscore) {
	"use strict";

	return Controller.extend("nNext.Flow7.Processing.controller.Fdp", {

		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			this._oRouter.getRoute("Write_Form").attachMatched(this._handleRouteMatched, this);
		},

		_handleRouteMatched: function(oEvent) {
			this.loadInitialData();
			this.loadErpData();
		},
		loadInitialData: function() {
			var oView = this.getView();

			var oModel =
				(new sap.ui.model.json.JSONModel())
				.attachRequestCompleted(function() {
					oView.setModel(oModel, "fdp");
				})
				.attachPropertyChange(function() {
					sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oView.getModel("fdp"));
				});
			oModel.loadData("/Flow7Api/api/fdp");
		},

		loadErpData: function() {
			var self = this;

			var oView = this.getView();
			oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/purma"), "purma");
			new sap.ui.model.json.JSONModel("/ErpApi/api/receipt").attachRequestCompleted(function(oData) {
				oView.setModel(oData.getSource(), "receiptAll");

				self.renderReceipt(oView.getModel("fdp").getProperty("/DocumentCategoryId"));
			});
		},

		onPurmaRequest: function() {
			if (!this._oPurmaDialog) {
				this._oPurmaDialog = sap.ui.xmlfragment("nNext.Flow7.Processing.view.Purma", this);
				this._oPurmaDialog.setModel(this.getView().getModel("purma"));
			}

			this._oPurmaDialog.open();
		},

		handlePurmaSelection: function(oEvent) {
			var oContexts = oEvent.getParameter("selectedContexts");

			if (oContexts && oContexts.length) {
				var oSelectedItem = oContexts[0].oModel.getProperty(oContexts[0].sPath);

				this.updateFdpData("PaySupplier", oSelectedItem.Ma001);
				this.updateFdpData("PaySupplierName", oSelectedItem.Ma003);
				this.updateFdpData("PaySupplierID", oSelectedItem.Ma005);
				this.updateFdpData("Currency", oSelectedItem.Ma021);
			}
		},

		updateFdpData: function(sKey, sValue) {
			var oModel = this.getView().getModel("fdp");

			oModel.setProperty("/" + sKey, sValue);
			this.getView().setModel(oModel, "fdp");
		},

		onPayTypeChange: function(oEvent) {
			var oView = this.getView();
			var sSelection = oView.byId("paytype").getSelectedKey();

			switch (sSelection) {
				case "其他":
					oView.byId("paytypeother").setVisible(true);
					break;
				default :
					oView.byId("paytypeother").setVisible(false);
					break;
			}
		},

		updateReceiptData: function() {
			this.updateFdpData("DocumentCategoryId", this.getView().byId("documentcategoryid").getSelectedKey());
			this.updateFdpData("DocumentCategory", this.getView().byId("documentcategoryid").getSelectedItem().getText());
			this.updateFdpData("InvoiceType", this.getView().byId("invoicetype").getSelectedKey());
			this.updateFdpData("TaxationType", this.getView().byId("taxationtype").getSelectedKey());
		},

		onReceiptChange: function(oEvent) {
			this.renderReceipt(parseInt(this.getView().byId("documentcategoryid").getSelectedKey()));
			this.updateReceiptData();
		},

		renderReceipt: function(nReceiptId) {
			var oView = this.getView();
			var oReceipt = oView.getModel("receiptAll").getProperty("/");
			oView.setModel(new sap.ui.model.json.JSONModel(_.findWhere(oReceipt, {
				"ReceiptId": nReceiptId
			})), "receipt");
		},

		updateFee: function() {
			var total = parseFloat(this.getView().byId("notaxamount").getValue().replace(/[^\d\.-]/g, "")) + parseFloat(this.getView().byId(
				"tax").getValue().replace(/[^\d\.-]/g, ""));

			this.updateFdpData("/Total", total)
		}
	});
});