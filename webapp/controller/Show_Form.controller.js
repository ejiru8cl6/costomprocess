sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nNext.Flow7.Processing.controller.Show_Form", {
		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(function(oEvent) {
				$.ajax("/Flow7Api/api/dashboard/processing/folder")
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
							console.log(oData);
						ctrl.getView().setModel(oData, "lists");
					});
			});
			this._oRouter.getRoute("Show_Form").attachMatched(this._handleRouteMatched, this);
		
		},

		_handleRouteMatched: function(oEvent) {
			this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].folderName);

			// var folderId = oEvent.getParameter("arguments").folderId;
			// var data = this.getOwnerComponent().getModel("diagrams").getData();

			// this.getView().setModel(
			// 	new sap.ui.model.json.JSONModel(_.findWhere(data, {
			// 		"FolderGuid": folderId
			// 	}).DiagramLists),
			// 	"lists");
		},
		onNavPress: function(oEvent) {
			this._oRouter.navTo("Show_Folder");
		},

		onItemPress: function(oEvent) {
			// var oObject = oEvent.getSource().getBindingContext("lists");
			// var oItem = oObject.getModel().getProperty(oObject.getPath());

			// this._oRouter.navTo("Write_Form", {
			// 	diagramId: oItem.Key.DiagramId
			// });
			var oObject = oEvent.getSource().getBindingContext("lists");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("Show_FormItem", {
				diagramId: oItem.Key.DiagramId,
				query: {
					diagramName: oItem.Key.DiagramName
				}
			});
		}
	});
});