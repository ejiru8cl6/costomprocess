sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"nNext/Flow7/Processing/model/models",
	"sap/ui/core/BusyIndicator",
	"sap/ui/core/routing/History"
], function(UIComponent, Device, models, BusyIndicator,History) {
	"use strict";

	return UIComponent.extend("nNext.Flow7.Processing.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			
			BusyIndicator.show();
			
			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			if (History.getInstance().getPreviousHash() === undefined) {
				this._oRouter.navTo("Show_Folder");
			}
		},
		onBeforeRendering: function() {
		}
	});
});